package com.example.demoSpringBoot.util;

public class RestClientUtilException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RestClientUtilException() {
		super();
	}

	public RestClientUtilException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RestClientUtilException(String message, Throwable cause) {
		super(message, cause);
	}

	public RestClientUtilException(String message) {
		super(message);
	}

	public RestClientUtilException(Throwable cause) {
		super(cause);
	}

}
