package com.example.demoSpringBoot.util;

import java.net.URI;
import java.util.Arrays;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


public class RestClientUtil {
	
	public static JSONObject getJsonFromWs(String wsUri, Map<String, String> uriPathParams,
			Map<String, String> queryParams, Map<String, Integer> properties) {

		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(wsUri);

		if (queryParams != null && !queryParams.isEmpty()) {
			// Set the query parameters
			for (String queryParanName : queryParams.keySet()) {
				builder.queryParam(queryParanName, queryParams.get(queryParanName));
			}
		}

		URI finalWsUri = builder.build().toUri();
		if (uriPathParams != null && !uriPathParams.isEmpty()) {
			finalWsUri = builder.buildAndExpand(uriPathParams).toUri();
		}

		System.out.println("Uri = "+ finalWsUri);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<String> response = null;
		
		response = retryCallEndpoint(properties, finalWsUri, requestHeaders, restTemplate, response);
		
		if(response != null) {
			
			System.out.println("Response status code = "+ response.getStatusCode());
			
			if (!response.getStatusCode().is2xxSuccessful()) {
				JSONObject error = new JSONObject(response.getBody());
				throw new RestClientUtilException(error.getString("message"));
			}
			
			return new JSONObject(response.getBody().trim());
		}else {
			System.err.println(new RestClientUtilException("Error to call: "+finalWsUri));
			throw new RestClientUtilException("Error to call: {}"+finalWsUri);
		}

	}
	
	private static ResponseEntity<String> retryCallEndpoint(Map<String, Integer> properties, URI finalWsUri,
			HttpHeaders requestHeaders, RestTemplate restTemplate, ResponseEntity<String> response) {
		try {
			response = getResponse(finalWsUri, requestHeaders, restTemplate);
		} catch (HttpServerErrorException restClient){
			if(restClient.getStatusCode().equals(HttpStatus.GATEWAY_TIMEOUT)) {
				System.out.println("GATEWAY TIMEOUT. Code: "+ HttpStatus.GATEWAY_TIMEOUT);
				for (int i = 0; i < properties.get("retryEndPoint");) {
					System.out.println("RERTY CALL ENDPOINT: "+finalWsUri);
					try {
						response = getResponse(finalWsUri, requestHeaders, restTemplate);
						if(response.getStatusCode().equals(HttpStatus.OK)) {
							break;
						}
					} catch (HttpServerErrorException e) {
						i++;
						if (e.getStatusCode().equals(HttpStatus.GATEWAY_TIMEOUT)
								&& i != properties.get("retryEndPoint")) {
							continue;
						} else {
							System.err.println("Error Trying to call EndPoint.");
							System.err.println("EndPoint not respond!!! " +e);
						}
					}
				}
			}
		}
		return response;
	}
	
	private static ResponseEntity<String> getResponse(URI finalWsUri, HttpHeaders requestHeaders,
			RestTemplate restTemplate) {
		ResponseEntity<String> response = restTemplate.exchange(finalWsUri, HttpMethod.GET,
				new HttpEntity<>(requestHeaders), String.class);
		return response;
	}

}
