package com.example.demoSpringBoot.service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.example.demoSpringBoot.util.CSVUtils;
import com.example.entity.EmployeeEntity;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@Service
public class EmployeesService {
	
	private Logger LOG = LoggerFactory.getLogger(EmployeesService.class);
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd");
	
	@Value("${environment.home}")
	private String environmentHome;
	
	@Value("${environment.user}")
	private String environmentUser;
	
	public void writeCSV(List<EmployeeEntity> data) throws Exception {
		String uploadPath = environmentHome+"/uploads";
		File directory = new File(uploadPath);
		
		if (! directory.exists()){
	        directory.mkdir();
		}
		
		String fileName = uploadPath+"/employees_"+sdf.format(new Date())+".csv";
		
		FileWriter writer = new FileWriter(new File(fileName), true);
		
		CSVUtils.writeLine(writer,
				Arrays.asList("id", "employee_name", "employee_salary", "employee_age", "profile_image"));
		
		for(EmployeeEntity employee: data){
			List<String> listData = new ArrayList<>();
			listData.add(employee.getId().toString());
			listData.add(employee.getEmployeeName());
			listData.add(employee.getEmployeeSalary().toString());
			listData.add(employee.getEmployeeAge().toString());
			listData.add(employee.getProfileImage());
			
			try {
				CSVUtils.writeLine(writer, listData);
			} catch (IOException e) {
				LOG.error("Error to write CSV. Cause: {}",e.getMessage(), e);
			}
		}
		writer.flush();
		writer.close();
		connectToSftp(fileName);
		boolean success = (new File(fileName)).delete();
		
		System.out.println("Local file deleted: "+success);
	}
	
	public void connectToSftp(String filePath) {

		String sftp_host = "34.68.51.83";
		int sftp_port = 22;

		JSch jSch = new JSch();
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			String privateKey = "private-key.pem";
			
			ClassLoader classLoader = new EmployeesService().getClass().getClassLoader();
			 
	        File file = new File(classLoader.getResource(privateKey).getFile());
			
	        jSch.addIdentity(file.getPath());
			LOG.info("Private Key Added.");
			
			session = jSch.getSession(environmentUser, sftp_host, sftp_port);
			LOG.info("session created.");
			
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			
			String remoteDirectory = channelSftp.pwd()+"/uploads/";
			channelSftp.cd(remoteDirectory);
			LOG.info("Changed the directory...");
			
			String[] fileName = filePath.split("/");
			
			channelSftp.put(filePath, remoteDirectory + fileName[fileName.length-1]);
			LOG.info("File uploaded successfull.");
			
		} catch (JSchException e) {
			e.printStackTrace();
		} catch (SftpException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (channelSftp != null) {
				channelSftp.disconnect();
				channelSftp.exit();
			}
			if (channel != null)
				channel.disconnect();

			if (session != null)
				session.disconnect();
		}
	}
	
}
