package com.example.demoSpringBoot.service;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.cloud.ReadChannel;
import com.google.cloud.storage.Storage;

@Service
public class FIleReaderService {
	
	@Autowired
	private Storage storage;
	
	public void gcsFiles(Map<String, Object> response) {
		StringBuilder sb = new StringBuilder();
		
		storage.list("test_gcs_fal").iterateAll().forEach(data -> {
			response.put("file_name", data.getName());
			try (ReadChannel channel = storage.reader("test_gcs_fal", data.getName())) {
				ByteBuffer bytes = ByteBuffer.allocate(64 * 1024);
				while (channel.read(bytes) > 0) {
					bytes.flip();
					String fileData = new String(bytes.array(), 0, bytes.limit());
					sb.append(fileData);
					bytes.clear();
				}
				response.put("contents", sb.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

}
