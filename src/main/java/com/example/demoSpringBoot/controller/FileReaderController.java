package com.example.demoSpringBoot.controller;


import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoSpringBoot.service.FIleReaderService;

@RestController
public class FileReaderController {
	
	@Autowired
	private FIleReaderService service;
	
	@RequestMapping(path = "/storage", method = {RequestMethod.GET})
	public ResponseEntity<Object> readFromFile() throws Exception {
		Map<String, Object> response = new LinkedHashMap<>();
		
		service.gcsFiles(response);

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}


}