package com.example.demoSpringBoot.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoSpringBoot.service.EmployeesService;
import com.example.demoSpringBoot.util.RestClientUtil;
import com.example.entity.EmployeeEntity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
@RequestMapping(value="/api/v1/employees")
public class EmployeesController {
	
	private Logger LOG = LoggerFactory.getLogger(EmployeesController.class);
	
	@Value("${url.dummy.employees}")
	private String dummyEndpoint; 
	
	@Autowired
	private EmployeesService service;
	
	@RequestMapping(value="/upload" ,method = RequestMethod.GET)
	public Object getEmployees(){
		Map<String, Object> responseMap = new LinkedHashMap<>();
		try {
			JSONObject response = RestClientUtil.getJsonFromWs(dummyEndpoint, null, null, null);
			JSONArray res = (JSONArray) response.get("data");
			Gson gson = new Gson();
			List<EmployeeEntity> list = new ArrayList<>();
			list = gson.fromJson(res.toString(),  new TypeToken<List<EmployeeEntity>>() {}.getType());
			
			service.writeCSV(list);
			
			LOG.info("{}", res);
			responseMap.put("message", "file upload successfull");
			return ResponseEntity.status(HttpStatus.OK).body(responseMap);

		} catch (Exception e) {
			responseMap.put("error", "error to call dummy endpoint");
			responseMap.put("cause", e.getMessage());
			responseMap.put("details", e);
			LOG.error("Error to call service. cause: {}", e.getMessage());
			return ResponseEntity.status(HttpStatus.CONFLICT).body(responseMap);
		}
	}

}
