package com.example.entity;

import lombok.Data;
import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

@Data
public class EmployeeEntity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@SerializedName("employee_name")
	private String employeeName;
	
	@SerializedName("employee_salary")
	private Long employeeSalary;
	
	@SerializedName("employee_age")
	private Long employeeAge;
	
	@SerializedName("employee_image")
	private String profileImage;

}
